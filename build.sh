#!/bin/sh

python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
sphinx-build -W -j auto -b html . public