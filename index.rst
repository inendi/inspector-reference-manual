.. Squey documentation master file, created by
   sphinx-quickstart on Fri Mar 31 12:01:55 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Squey documentation!
============================================

.. image:: /_static/images/views_small.png
   :width: 1200

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   :glob:

   content/introduction/*
   content/concepts/*
   content/startscreen/*

   content/views/*
   content/listing_view/*
   content/global_pc_view/*
   content/zoomed_pc_view/*
   content/scatter_plot_view/*
   content/hit_count_view/*
   content/series_view/*
   content/statistics_view/*
   content/layer_stack_view/*

   content/filters/*
   content/editors/*

   content/format_builder/*
   content/export_data/*
   content/correlation/*
   content/python_scripting/*


Appendix
========

.. toctree::
   :maxdepth: 1
   :caption: Appendix:
   :glob:

   content/menu/*
   content/windows_installer_error_codes/*

