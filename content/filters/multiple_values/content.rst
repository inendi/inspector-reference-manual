.. _Multiple Values Search Filter:

Multiple Values Search Filter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :ref:`Multiple Values Search Filter` is a text based selection :ref:`Filter`. It does not change the color of :ref:`Event`\ s. Its purpose is to select, from the current :ref:`Selection` of :ref:`Event`\ s, all the :ref:`Event`\ s that carry a value, on a given :ref:`Axis`, that belongs to a list of patterns provided by the user.

The :ref:`Multiple Values Search Filter` looks like this:

.. image:: /_static/images/multiple_values.png
   :width: 600

To use this :ref:`Filter`, there are some parameters and options available to the user. We now present them shortly.

Parameters
^^^^^^^^^^

Case sensitivity
++++++++++++++++

This parameter controls either the match must consider case or not.

.. image:: /_static/images/multiple_values_param01.png
   :width: 500

Matching rule
+++++++++++++

This parameter controls either the match considers the whole field must or not.

.. image:: /_static/images/multiple_values_param02.png
   :width: 400

Expression type
+++++++++++++++

This parameter controls either the expression is a constant string to match or a regular
expression.

.. image:: /_static/images/multiple_values_param03.png
   :width: 500

.. warning::

   Regular expressions are way slower than plain text because of the inherent cost of regular expressions, but also because fields internal representations can no longer be used for doing the comparisons.

Expression
++++++++++

This parameter controls the expression to use for the search. If the expression contains
more than one line, the result of the search will be the union of the result of the search
using each line.

.. image:: /_static/images/multiple_values_param04.png
   :width: 400

Search mode
+++++++++++

This parameter controls either the expression is used to accept events or is used to ignore
them.

.. image:: /_static/images/multiple_values_param05.png
   :width: 500

Axis
++++

This parameter enables to choose the considered axis.

.. image:: /_static/images/multiple_values_param06.png
   :width: 400

Access
^^^^^^

There are two ways to access this filter.

By using the 'Multiple values...' menu entry:

.. image:: /_static/images/access_multiple_values_01.png
   :width: 400

By right-clicking on a cell in the listing view:

.. image:: /_static/images/access_multiple_values_02.png
   :width: 400

There are two ways to use this filter.

To run immediatly the search using a value present in the listing, right-click on the
wanted value's cell to open the context menu and select the 'search for this value' action:

.. image:: /_static/images/fast_access_multiple_values_01.png
   :width: 400

To open the search dialog using a value present in the listing, right-click on the
wanted value's cell to open the context menu and select the 'search using this value...' action:

.. image:: /_static/images/fast_access_multiple_values_02.png
   :width: 400

