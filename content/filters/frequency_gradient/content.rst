
.. _Frequency Gradient Filter:

Frequency Gradient Filter
~~~~~~~~~~~~~~~~~~~~~~~~~


The :ref:`Frequency Gradient Filter` is a simple mathematical :ref:`Filter` that selects :ref:`Event`\ s and colorize them according to the frequency at which values taken by all the selected :ref:`Event`\ s occur on a given :ref:`Axis`.



.. figure:: /_static/images/frequency_gradient_dialog.png
   :width: 400

   Dialog of the frequency gradient filter

.. figure:: /_static/images/frequency_gradient_01.png
   :width: 1000

   A parallel view using the default event color

.. figure:: /_static/images/frequency_gradient_02.png
   :width: 1000

   The same parallel view using a frequency gradient on the 'Result Code' axis





Parameters
^^^^^^^^^^

This filter's dialog gives access to three parameters.

Axis
++++
The first parameter enables to choose the considered axis. It is controlled using
a combo-box.

.. image:: /_static/images/frequency_gradient_param01.png
   :width: 300

Scale type
++++++++++

The second one enables to choose between a linear or a logarithmic scale to map the
frequences to the resulting color. It is controlled using a combo-box.

.. image:: /_static/images/frequency_gradient_param02.png
   :width: 300

Frequency range
+++++++++++++++

The last parameter enables to graphically define the frequency range for which
the gradient will be applied to. The lower and upper bounds handles can be
dragged as usual sliders. The bounds can be set more precisely using the two
spinboxes. To set the range fastly (but with less precision), the lower bound
can be changed by left-clicking in the color-ramp and the upper bound can be
changed by right-clicking in the color-ramp.

.. image:: /_static/images/frequency_gradient_param03.png
   :width: 300

Access
^^^^^^

There are one way to access this filter.

By using the 'Frequency gradient...' menu entry:

.. image:: /_static/images/access_frequency_gradient_01.png
   :width: 300
