
.. _Axis Gradient Filter:

Axis Gradient Filter
~~~~~~~~~~~~~~~~~~~~

.. figure:: /_static/images/axis_gradient_dialog.png
   :width: 400

   Dialog widget of the Axis Gradient Filter


The aim of this :ref:`Filter` is to colorize :ref:`Event`\ s according to the value they carry on one of their dimension. The idea is very simple: suppose the third :ref:`Axis` carries the hour of the :ref:`Event`, from 00:00:00 (at the very bottom of the :ref:`Axis`) to 23:59:59 (at the very top of the :ref:`Axis`). If the third :ref:`Axis` is used to apply an :ref:`Axis Gradient Filter`, then a green-to-red color gradient will be used to map the hour to a particular color:

- :ref:`Event`\ s get a green color if they happen early during the day.
- :ref:`Event`\ s get a yellow color if they happen around noon.
- :ref:`Event`\ s get a red color if they happen by the end of the day.


Let's have a look at a real example! We start by a selection of :ref:`Event`\ s that have no color indication:

.. figure:: /_static/images/axis_gradient_01.png
   :width: 1000

   A parallel view using the default event color

We can apply an :ref:`Axis Gradient Filter` to the first :ref:`Axis` (the _Time_ :ref:`Axis` in our case):

.. figure:: /_static/images/axis_gradient_02.png
   :width: 1000

   The same parallel after applying an Axis Gradient Filter on the 'Time' Axis



This :ref:`Axis Gradient Filter` is usefull if you want to correlate positions (i.e. values) of :ref:`Event`\ s on a specific :ref:`Axis` (the third one in our example) with one :ref:`Axis` or possibly many other :ref:`Axis`. Without beeing very accurate, the color information given to the :ref:`Event`\ s by the color gradient is a very easy way to get an intuitive feeling of possible links between the selected :ref:`Axis` and the other Axes.





Parameters
^^^^^^^^^^

This filter's dialog gives access to one parameter: the considered axis which is controlled
using a combo-box.

.. image:: /_static/images/axis_gradient_param01.png
   :width: 400

Access
^^^^^^

There are two ways to access this filter.

By using the 'Axis gradient...' menu entry:

.. image:: /_static/images/access_axis_gradient_01.png
   :width: 400

By right-clicking on a column in the listing view:

.. image:: /_static/images/access_axis_gradient_02.png
   :width: 400
