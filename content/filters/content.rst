.. _Filters:

Filters
-------


In Squey, :ref:`Filter` is a generic term that refers to a computation or a filtering process. It applies to a part or to all the data being investigated and might produce a change on the two following items:

- the current :ref:`Selection` of selected :ref:`Event`\ s.
- the color of selected :ref:`Event`\ s in the current active :ref:`Layer`.

.. note:: About visible Layers

   Remember that there is a fundamental distinction between:
     * the visible :ref:`Layer`\ s that make up the visible :ref:`Event`\ s. These visible :ref:`Layer`\ s are the one that are checked as "visible" in the :ref:`Layer Stack View`.
     * the active :ref:`Layer`, which is the only :ref:`Layer` in the :ref:`Layer Stack View` that has the focus on it.

.. image:: /_static/images/layer_stack_01.png
   :width: 300

One should always keep in mind that the :ref:`Event`\ s that might get a change in their color property are not necessarily all the :ref:`Event`\ s that appear visible on screen, but only the :ref:`Event`\ s that are, at the same time, visible and in the active :ref:`Layer`.

Conclusion: to put things in simple terms:

.. warning::
  :ref:`Filters` take their input data from all the visible and selected :ref:`Event`\ s and may or may not change:
   * the current :ref:`Selection`.
   * the color property of the visible :ref:`Event`\ s in the active :ref:`Layer`.


.. _Preset:

Presets of Filters
~~~~~~~~~~~~~~~~~~

Regular users of Squey often need to save and reuse specific settings of some :ref:`Filters` they need to apply frequently on a well known category of data or :ref:`Event`\ s. To satisfy that need, :ref:`Preset`\ s of :ref:`Filters` are available on all :ref:`Filters` of Squey.

This common feature of :ref:`Filters` is positionned at the end of each :ref:`Filter` widget and looks like this:


.. image:: /_static/images/presets_box.png
   :width: 500



The two following icons make it possible to save or load :ref:`Preset`\ s:

.. |ico1| image:: /_static/images/presets_load.png 

- |ico1| : use this icon when you want to load an existing :ref:`Preset`.

.. |ico2| image:: /_static/images/presets_save.png

- |ico2| : use this icon when you want to save the actual :ref:`Filter` settings as a :ref:`Preset` ready to be reused.

The text area shows the actual existing :ref:`Preset`\ s that can be selected and loaded. If no :ref:`Preset` exists, this area is empty.


.. figure:: /_static/images/frequency_gradient_presets.png
   :width: 400
  
   Sample of presets for the frequency gradient filter

Through the list of :ref:`Preset`\ s, it is possible to rename or remove (i.e. delete) an existing :ref:`Preset`. A right-click on such an existing :ref:`Preset` offers the following possibilities:

- Load: use this if you want to load the settings held by that :ref:`Preset`.
- Save: use this menu entry *only* if you want to overwrite the settings of this existing :ref:`Preset`.
- Rename: self explanatory.
- Remove: use this menu entry to delete the targeted existing :ref:`Preset`.

Finally, it is also possible to load an existing :ref:`Preset` by a double-click on the :ref:`Preset`'s name in the list.


.. note:: In the current version of Squey, :ref:`Preset`\ s are saved on a System Wide basis: existing :ref:`Filters` are available globally, for all users of Squey.





.. Included subsections
.. include:: axis_gradient/content.rst
.. include:: frequency_gradient/content.rst
.. include:: multiple_values/content.rst
