Introduction
------------


Purpose of this manual
~~~~~~~~~~~~~~~~~~~~~~

The aim of the Squey Reference Manual is to provide an exhaustive and in-depth presentation of all functionalities of the Squey software.

However, this manual will not give any hint on how to conduct a real investigation on a given dataset. In particular, it does not focus on the usefulness of given aspects of Squey relatively to given methodologies of investigations.




