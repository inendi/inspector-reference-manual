
Menu entries
------------

File menu
~~~~~~~~~

Investigation
^^^^^^^^^^^^^

New Investigation
+++++++++++++++++

Create a Squey window to manage an investigation.

Load an investigation
+++++++++++++++++++++

Load an investigation from a file in the current Squey window. The current investigation
is replaced with the new one.

Save investigation
++++++++++++++++++

Save the current investigation. If it is a new one, a target file name will be asked for; otherwise
the origin file name will be used.

Save investigation as...
++++++++++++++++++++++++

Save the current investigation by asking for a new file name.

Data collection
^^^^^^^^^^^^^^^

New data collection
+++++++++++++++++++

Add a new data collection to the current investigation.

Import
^^^^^^

Local files...
++++++++++++++

Load events from a list of files locally stored. "local" means seen by the operating system as a
regular file (disk partitions, NFS, Samba/CIFS).

Remote files...
+++++++++++++++

Load events from a remote files. "remote" means the file is read through a given protocol.

Database...
+++++++++++

Load events from the result of a database query.

Export
^^^^^^

Export selection...
+++++++++++++++++++

Save the current selection as a CSV local file.

Quit
^^^^

Close the current Squey window.

Selection menu
~~~~~~~~~~~~~~

Select all events
^^^^^^^^^^^^^^^^^

Select all events.

Empty selection
^^^^^^^^^^^^^^^

Clear the current selection.

Invert selection
^^^^^^^^^^^^^^^^

Invert the current selection in the set of all selectable events.

Set selection from current layer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Select all events which are selectable in the current layer.

Set selection from layer...
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Select all events which are selectable in a given layer.

Set color
^^^^^^^^^

Set the color of the selected events in the current layer.

Create new layer from selection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create a new layer whom selectable events are those in the current selection.

Move selection to new layer
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Remove the current selection from the current layer's selectable event set and set them as
selectable in the target layer.

Expand selection on axis...
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Recompute the mapping of an axis to make the current selection fit it entirely.

Filters menu
~~~~~~~~~~~~

Apply last filter...
^^^^^^^^^^^^^^^^^^^^

Open the last runned filter's dialog with the used parameters.

Axis gradient...
^^^^^^^^^^^^^^^^

Open the 'Axis gradient''s dialog.

Frequency gradient...
^^^^^^^^^^^^^^^^^^^^^

Open the 'Frequency gradient''s dialog.

Text search
^^^^^^^^^^^

Multiple values...
++++++++++++++++++

Open the 'Multiple values' text search's dialog.

Source menu
~~~~~~~~~~~

Create new source from input...
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Open the 'extractor' dialog to create a new source from the current input.

Display invalid events...
^^^^^^^^^^^^^^^^^^^^^^^^^

Open a dialog displaying the invalid events of the current source.

View menu
~~~~~~~~~

Edit axis combination...
^^^^^^^^^^^^^^^^^^^^^^^^

Open the 'axis combination editor'.

Lines menu
~~~~~~~~~~

Toggle unselected events in listing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set either unselected events are visible in the listing view or not.

Toggle zombie events in listing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set either zombies events are visible in the listing view  or not.

Toggle unselected and zombie events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set either unselected events and zombies events are visible in all views  or not.

Tools menu
~~~~~~~~~~

New format...
^^^^^^^^^^^^^

Open the 'format builder' to create a new format.

Edit current format...
^^^^^^^^^^^^^^^^^^^^^^

Open the 'format builder' to edit the current format.

Help menu
~~~~~~~~~

Open the 'about' dialog.
