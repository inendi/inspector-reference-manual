.. _Layer Stack View:

Layer Stack View
----------------

The :ref:`Layer Stack View` is the widget that manages all existing :ref:`Layer`\ s of a :ref:`View`. By default, it is located at the bottom-right corner of the application's window.

.. image:: /_static/images/layer_stack_01.png
   :width: 300

:ref:`Layer`\ s priority is increasing from bottom to top. It means that the first visible :ref:`Layer` is the one on top of the :ref:`Layer Stack View` and that the less visible :ref:`Layer` is the one at the bottom of the :ref:`Layer Stack View`.

This hierarchy of :ref:`Layer`\ s has a direct impact on some graphical representations. For example, on the :ref:`Global Parallel Coordinates View`, the color of a visible line is strongly dependent of the :ref:`Layer`\ s' order in the :ref:`Layer Stack View`. In particular, this means that if a line exists in the top-most :ref:`Layer` and has color red in that :ref:`Layer`, and if the same line exists in another :ref:`Layer` (necessarily below the top-most), with any given color, the same line will be visible in the :ref:`Global Parallel Coordinates View` in red.

This means that there is no color blending of lines between :ref:`Layer`\ s: the top-most color overwrites all lower colors!

For each :ref:`Layer` in the :ref:`Layer Stack View`, three piece of information are displayed:

* The 'visibility status' of the :ref:`Layer`. When a layer is not visible, lines that it contains are rendered as 'Zombie Lines' and cannot be selected.
* The name of the layer
* The number of events in the layer

We will now detail all the functionalities of the Layer Stack widget.


Contextual menu
~~~~~~~~~~~~~~~

A right-click on a layer gives access to it's Contextual Menu.

.. image:: /_static/images/layer_stack_menu_02.png
   :width: 400

From there, the following actions can be taken:

* 'Set selection from this layer content...'
  Set the current selection to the content of a layer.
* 'Export this layer selection'
  Export the current layer selection in a file.
* 'Reset this layer's colors to white'
  Reset to white the lines colors of the current layer.
* 'Show this layer only'
  Hide all the other layers.
* '(Boolean operations)'
  Perform the selected boolean operation between the current selection and the selected layer
* 'Copy the layer stack's details to clipboard'
  Copy to the clipboard the list of layers and their event counts as a tab separated values text.


Creating a new layer
~~~~~~~~~~~~~~~~~~~~

There are three ways to create a new layer.

.. image:: /_static/images/new_layer_name.png
   :width: 300

You can either:

* Use the Application Menu
  Go to the *Selection* menu, then to the *Create new layer from selection* entry.
* Use a shortcut
  You can type +Alt+k+ to create a next layer from the current selection
* Click on the 'New layer' Icon of the Layer Stack.

Copying the current selection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By clicking 'Create new layer from selection' on the 'Selection' menu (or using its associated
'Alt+K' global shortcut).

.. image:: /_static/images/new_layer_copying_selection_menu.png
   :width: 300

Moving the current selection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By clicking 'Move selection to new layer' on the 'Selection' menu (or using its associated 'Alt+M'
global shortcut).

.. image:: /_static/images/new_layer_moving_selection_menu.png
   :width: 300

From the complete selection
^^^^^^^^^^^^^^^^^^^^^^^^^^^

By clicking on the 'New layer' button located at the bottom of the layer stack widget.

.. image:: /_static/images/new_layer_from_layer_stack.png
   :width: 300

From an existing layer
^^^^^^^^^^^^^^^^^^^^^^

By selecting the wanted layer and by clicking on the 'Duplicate selected layer' button located at
the bottom of the layer stack widget.

.. image:: /_static/images/duplicate_layer.png
   :width: 300

Inverting the selection after a layer creation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By clicking 'Set selection from this layer's content' on the wanted layer in the layer stack widget.

.. image:: /_static/images/set_selection_from_layer_content.png
   :width: 600

Accessing the view
~~~~~~~~~~~~~~~~~~

.. image:: /_static/images/layerstack_access.png
   :width: 300
