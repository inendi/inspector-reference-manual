.. _Listing View:

Listing View
------------

The purpose of the :ref:`Listing View` is to present the data in its textual representation. Therefore, it is a tabular representation of the original data with one line of the table dedicated to one :ref:`Event` of the dataset, and one :ref:`Column` of the table dedicated to one :ref:`Field` of the dataset. Each value of a :ref:`Field` in a given :ref:`Event` has a textual representation (i.e. as a string) that is shown as a listing in a given cell of a table.

The :ref:`Listing View` has the following aspect:


.. image:: /_static/images/listing_view1.png
   :width: 1200

On the left margin, each row (i.e. line) of the table is referenced by a number which is its row index in the imported dataset. In the upper margin, each column inherits a title that has been defined by the user in the :ref:`Format` used at import time for the original data.

The bottom part of the :ref:`Listing View` optionaly shows the :ref:`Statistics Panel` associated to the data. This :ref:`Statistics Panel` will be described in a later section of this document but we can precise right now that its main purpose is, by default, to show the number of distinct values in each column of the listing.

A line index can be copied to the clipboard by doing a right click on the left margin.

.. image:: /_static/images/copy_line_index.png
   :width: 200

By pressing the 'g' key, a dialog allows to go to a given line index. If the line index refers to an unselected :ref:`Event`, the nearest selected one will be used.

.. image:: /_static/images/goto_line.png
   :width: 200


Selected, Unselected and Zombie Events
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As Squey is all about investigating and diving into large datasets of :ref:`Event`\ s, it often happens that one focuses on subsets of the whole dataset. As the investigation goes on, more subsets are created. During the analysis made through Squey, it is natural for the user to have some subsets active and some other inactive.

In the terminology of Squey, these subsets are associated with :ref:`Layer`\ s. (A :ref:`Layer` is a selection of a subset of :ref:`Event`\ s and, for each of these :ref:`Event`\ s, a color property).

If we consider a specific :ref:`Event` of the global dataset (the imported :ref:`Source`), at a given moment in time, it can be in one of the three mutually exclusive states:

- The :ref:`Event` can be *selected*, meaning that it belongs to one of the active :ref:`Layer`\ s in the :ref:`Layer Stack View` and it also belongs to the current selection.
- The :ref:`Event` can be *unselected*, meaning that it belongs to one of the active :ref:`Layer`\ s in the :ref:`Layer Stack View` but it does not belong to the current selection.
- The :ref:`Event` can be *zombie*, meaning that it does not belong to any of the active :ref:`Layer`\ s in the :ref:`Layer Stack View`.

This distinction between the different status of an :ref:`Event` is fundamental. Every user of Squey should be comfortable with it.


In the :ref:`Listing View`, the distinction is very easy to observe:

- *Selected* :ref:`Event`\ s have bright colors (Saturation and Value is at there maximum).
- *Unselected* :ref:`Event`\ s have shaded colors.
- *Zombie* :ref:`Event`\ s are in black.

By default, Zombie :ref:`Event`\ s are not shown in the :ref:`Listing View`.



.. _Statistics Panel:

Statistics Panel
~~~~~~~~~~~~~~~~


The :ref:`Statistics Panel` located at the bottom of the :ref:`Listing View` provides a convenient way to quickly access
informations about :ref:`Column`\ s, such as 'distinct values' or 'sum'.

.. image:: /_static/images/listing_view2.png
   :width: 1200

Distinct values dialog visibility can be toggled by clicking on the cell icon.

Right-clicking on the vertical header opens up a contextual menu allowing to chose which tools are enabled or disabled (sum, min, max, average).

.. image:: /_static/images/stats_panel_tools.png
   :width: 200

Cell values can be copied to clipboard using the contextual "Copy" menu.

.. image:: /_static/images/copy_cell_value.png
   :width: 200

The panel visibility can be toggled by clicking on the "Toggle stats panel visibility" button.


.. |ico1| image:: /_static/images/stats_panel_on.png
.. |ico2| image:: /_static/images/stats_panel_off.png

|ico1| |ico2|

Resizing columns
~~~~~~~~~~~~~~~~

Resizing :ref:`Column`\ s to properly fit their content can be done by moving the column header border or
using the 'control'+'mouse wheel' shortcut.

.. note::

   A cell content that may not fit in the current cell width can nevertheless be fully displayed by leaving the mouse over the cell for a few seconds. The full content pops up as a toolip.


Header context menu
~~~~~~~~~~~~~~~~~~~

The context menu of the listing's header allows to create graphical and statistics :ref:`View`\ s on a given
:ref:`Column` as well as sorting the values of the :ref:`Column`.

.. figure:: /_static/images/header_context_menu.png
   :width: 400

   The context menu of the listing's header


Cell context menu
~~~~~~~~~~~~~~~~~

The context menu of the listing's cells allows to:

* Apply an :ref:`Axis Gradient Filter` on the axis.
* Filter the values of the column:
  #. 'Search using this value...': displays the search dialog filled with the content of the selected cell.
  #. 'Search for this value': filters the column using the content of the selected cell.
  #. 'Search for': displays an empty search dialog.
* Copy the content of the selected cell to the clipboard.
* Set a color to the selected events.

.. figure:: /_static/images/cell_context_menu.png
   :width: 400

   The context menu of the listing's cells



Selecting events
~~~~~~~~~~~~~~~~

Selecting :ref:`Event`\ s from the :ref:`Listing View` means commiting a selection to the current :ref:`Layer`. There are
several ways to select :ref:`Event`\ s from the :ref:`Listing View`:

. Select a single event by double-clicking on its line number.
. Highlight several events and press Enter.
. Use the contextual menu on a cell to filter events using given criterias.



Visual synchronisation with the parallel view
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Placing the mouse over a column title highlights its associated axis title and helps keeping track
of the association column/axis. +
Note that this highlight works in both ways, thus placing the mouse
over an axis titles highlights its associated column borders.

.. image:: /_static/images/visual_synchronisation.png
   :width: 600

Clicking on a column title translates the parallel view so that the associated axis is being placed
on top of it (if the parallel view is located on top of the listing view and are starting at the
same horizontal screen coordinate). Clicking on an axis title translates the listing view so the
associated column becomes visible.



How to access this View
~~~~~~~~~~~~~~~~~~~~~~~

.. image:: /_static/images/listing_access.png
   :width: 300
