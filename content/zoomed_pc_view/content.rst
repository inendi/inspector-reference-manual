.. _Zoomed Parallel Coordinates View:

Zoomed Parallel Coordinates View
--------------------------------

The clever user has probably noticed, from the previous section, that the :ref:`Global Parallel Coordinates View` offers no possibility to zoom vertically. The main reason to this limitation lies in the fact that the :ref:`Global Parallel Coordinates View` is aimed at providing a global representation: zooming on a detail of this :ref:`View` will remove from the user's eyes the panorama provided by the :ref:`Global Parallel Coordinates View`.

However, as far as it is very important to get an idea of the exact positions of values on a given :ref:`Axis`, Squey supports the :ref:`Zoomed Parallel Coordinates View`. This :ref:`View` will provide the user the possibility to easily zoom (on or out) of an :ref:`Axis`.

A typical :ref:`Zoomed Parallel Coordinates View` looks like this:

.. image:: /_static/images/zoomed_parallel_view.png
   :width: 700


A :ref:`Zoomed Parallel Coordinates View` is attached to a single :ref:`Axis` of the data. This :ref:`Axis` is shown vertically, right at the center of the :ref:`Zoomed Parallel Coordinates View`. On the left part of this :ref:`View`, the lines coming from the previous :ref:`Axis` of the :ref:`Axes combination` (of the current :ref:`Investigation`), are shown. Symetrically for the right part of the :ref:`Zoomed Parallel Coordinates View` and the next :ref:`Axis` in the :ref:`Axes combination`.

However, in the :ref:`Zoomed Parallel Coordinates View`, the scaling of :ref:`Event`\ s is done with one main restriction:

.. warning:: The :ref:`Event`\ s shown in the :ref:`Zoomed Parallel Coordinates View` are only the ones that have a value on the central :ref:`Axis` that fails in the portion of the central :ref:`Axis` that is currently visible.


This mean that the plot seen in the :ref:`Zoomed Parallel Coordinates View` is not an exact copy of what is seen in the :ref:`Global Parallel Coordinates View`. For example, a segment of an :ref:`Event` that crosses the squarred zone delimited by the :ref:`Zoomed Parallel Coordinates View` but hits the central axis slightly outside the visible part of that central :ref:`Axis` in the :ref:`Zoomed Parallel Coordinates View` will *not* be rendered.

This means that the :ref:`Zoomed Parallel Coordinates View` focuses on the :ref:`Event`\ s that somehow belong to the current portion of the central :ref:`Axis` that is attached to it (and according to the current zoom-factor in that View).



When a :ref:`Zoomed Parallel Coordinates View` is created, a pair of cursors appears on the corresponding :ref:`Axis` in the
:ref:`Global Parallel Coordinates View`. These cursors indicate the part of the concerned :ref:`Axis` displayed in the :ref:`Zoomed Parallel Coordinates View`, and they are updated according to the evolutions of the zoom-factor in the :ref:`Zoomed Parallel Coordinates View`.

These cursors can also be moved using the mouse; In this case, The :ref:`Zoomed Parallel Coordinates View`'s viewport is automatically adjusted to the nearest compatible configuration.


.. figure:: /_static/images/full_parallel_view_ref.png
   :width: 1200

   The corresponding zoom cursors in the Global Parallel Coordinates View

In addition to the common mouse behavior, the :ref:`Zoomed Parallel Coordinates View` has two extra panning modes
which are accessible with the mouse's wheel:

* if the 'shift' key is pressed, the view is panned one pixel at a time;
* if the 'control' key is pressed, the view is panned one step at a time;



Selection tool
~~~~~~~~~~~~~~

Contrary to others views which use a rectangle as selection tool, the zoomed parallel view uses a
dimension line to select event along the visualized axis.

Press the left mouse's button to set the first extension line. Move to the wanted position and
release the mouse's button to set the second extension line. While moving the mouse, the dimension
line is orange to indicate the current selection has not been already updated; when it becomes red,
the current selection has been updated.

.. figure:: /_static/images/zoomed_parallel_view_sel_create.png
   :width: 700

   Selection tool while creating it

After the left mouse's button has been released, the dimension line is replaced with movable
cursors like those of the full parallel view except the fact they are red and deleted the next
time a new selection is done using the dimension line.

.. figure:: /_static/images/zoomed_parallel_view_sel_editable_01.png
   :width: 700

   Corresponding movable cursors after having finished the selection

.. figure:: /_static/images/zoomed_parallel_view_sel_editable_02.png
   :width: 1200

   Corresponding movable cursors in the full parallel view


Options
~~~~~~~
Selecting the displayed axis
  To select the axis to display


Accessing the view
~~~~~~~~~~~~~~~~~~

There are three ways to access this graphic view.

By right-clicking on the axis header on the parallel view:

.. image:: /_static/images/create_zoomed_parallel_view_01.png
   :width: 400

By right-clicking on the header of column in the listing view:

.. image:: /_static/images/create_zoomed_parallel_view_02.png
   :width: 500

By clicking on the hit count view button located on the workspace toolbar and selection the appropriate axis:

.. |ico1| image:: /_static/images/create_zoomed_parallel_view_03a.png
.. |ico2| image:: /_static/images/create_zoomed_parallel_view_03b.png

|ico1| |ico2|


