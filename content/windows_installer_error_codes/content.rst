
Windows installer error codes
-----------------------------

.. _error-1000:

Error 1000
~~~~~~~~~~

Your OS needs to be one of the following (or newer) to support `WSL2 <https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux>`_ : 

* Windows 10 64 bits version 2004
* Windows Server 2019

.. _error-1001:

Error 1001
~~~~~~~~~~

Enabling Microsoft Windows `WSL2 <https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux>`_ feature is required in order to install and run Squey.

To enable it, go to "Settings > System > Optional features > More Windows features" and select:

* Virtual Machine Platform
* Windows Subsystem for Linux

.. image:: /_static/images/enable_wsl2.png

Or as a Windows administrator, run the following commands :

.. code-block:: batch

    dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
    dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

.. note::
    Reboot your computer to finish enabling WSL2 before installing Squey.

.. _error-1002:

Error 1002
~~~~~~~~~~

`WSLg <https://github.com/microsoft/wslg>`_ does not seem to be supported by your `WSL2 <https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux>`_ version.

Please, try installing the `latest WSL2 version from the Microsoft Store <https://apps.microsoft.com/detail/9p9tqf7mrm4r>`_.


