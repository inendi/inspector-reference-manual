.. _Series View:

Series View
-----------

This visualization displays different sets of metrics evolving over a common dimension.
Each numeric axis of the parallel coordinates view can therefor be displayed as a serie. Thereby, a serie is dependant of the :ref:`Scaling` of its axis.

.. image:: /_static/images/series_view.png
   :width: 800


Zoom
~~~~

Zoom level over the common dimension (X-axis) can be changed using the mouse wheel button or by doing a click-and-drag.
An homothetic zoom can be done using the "Ctrl" modifier both with the mouse wheel button or with a click-and-drag.

It is also possible to zoom between two values using the range widget :

.. image:: /_static/images/series_view_calendar.png
   :width: 400

Selection
~~~~~~~~~

Selecting a continuous range of values over the common dimension (X-axis) by doing a "Shift" + click-and-drag will instantly transfert the selection to the others views.

.. image:: /_static/images/series_view_zoomed.png
   :width: 800

The current selection of the :ref:`Source` has also an impact over the display of the timeseries : only the selected values are taken into account an the view is refreshed instantly upon any selection change.

.. image:: /_static/images/series_view_selection01.png
   :width: 500

.. image:: /_static/images/series_view_selection02.png
   :width: 500

Rendering modes
~~~~~~~~~~~~~~~

Lines
^^^^^

This is the default mode. It allows to easily spot gaps in data.

.. image:: /_static/images/series_view_lines.png
   :width: 500

Points
^^^^^^

This alternative mode can be interesting when the values are a bit chaotic.

.. image:: /_static/images/series_view_points01.png
   :width: 500

Lines Always
^^^^^^^^^^^^

This mode is advisable when the values are widely spaced and an interpolation is needed. 

.. image:: /_static/images/series_view_points02.png
   :width: 500

.. image:: /_static/images/series_view_lines_always.png
   :width: 500

Splitting the series according to a specific axis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It can sometimes be convenient to split one or more series according to the values contained in a specific column in order to reveal information that was previously hidden.

The resulting series are then superposed allowing to easily spot abnormal behavior.

.. image:: /_static/images/series_view_unsplit.png
   :width: 500

.. image:: /_static/images/series_view_split.png
   :width: 500

Any column can be used to split the series by selecting it in the proper combo box.

Selecting the series to display
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A click-and-drag over the list of series can toggle activate or deactivate several series at a time.

.. image:: /_static/images/series_view_list.png
   :width: 500


Accessing the view
~~~~~~~~~~~~~~~~~~

There are three ways to access this graphic view.

By right-clicking on the header of a zone on the parallel view:

.. image:: /_static/images/create_series_view_01.png
   :width: 400

By right-clicking on the header of column in the listing view:

.. image:: /_static/images/create_series_view_02.png
   :width: 500

By clicking on the scatter view button located on the workspace toolbar and selection the
appropriate zone:

.. |ico1| image:: /_static/images/create_series_view_03a.png
.. |ico2| image:: /_static/images/create_series_view_03b.png

|ico1| |ico2|


The selected axis will then be used a the common dimension (X-axis), and all the other numeric axes will be displayed a series (Y-axis).
