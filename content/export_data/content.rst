Exporting data
--------------

This chapter explains how to export various kind of data out of Squey.

Exporting selection
^^^^^^^^^^^^^^^^^^^

.. image:: /_static/images/export_selection.png
   :width: 1000

The following options can be specified:

#. export column names as header: Specify whenever the exported CSV data contains the names of the axes as first line, starting with a # (hash) character. You can then choose to ignore this header if the CSV needs to be imported back into Squey.
#. Separator/quote characters
#. custom axes combination: You can choose an alternate axes combination to remove or reorder the exported columns.
#. chose compression type (gz, bz2, zip or none)

Exporting the selection can be done by the global menu on the current selection:

.. image:: /_static/images/export_selection_menu.png
   :width: 500

or by using the context menu on a specific layer:

.. image:: /_static/images/export_selection_context_menu.png
   :width: 500

.. _Exporting statistics:

Exporting statistics
^^^^^^^^^^^^^^^^^^^^

Selected values can be exported as CSV using the contextual menu "Copy values". Chosing to export the count values currently shown (occurence count, scientific notation, percentage) as well is up to the user.

.. image:: /_static/images/statistical_views_export_values_with_count.png
   :width: 500

A line separator can as well be specified in order to export as a plain list or as a CSV file for
example (default is carriage return).

Alternatively, the whole list of values can be copied to the clipboard or to a file using one of the three buttons located on the
bottom of the dialog.

.. image:: /_static/images/statistical_views_export_values.png
   :width: 500

When doing so, a message box will ask the user if they want to export the count informations as well.

.. image:: /_static/images/statistical_views_export_values_msgbox.png
   :width: 500

CSV output is as follow when count informations are wanted:

.. code-block:: text
  
   "200","4,923,987","49.2%"
   "407","2,883,826","28.8%"
   "304","1,048,051","10.5%"
   "403","901,557","9.0%"
