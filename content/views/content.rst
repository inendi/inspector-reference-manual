.. _Views in Squey:

Views in Squey
--------------

Squey is a graphically oriented investigation software on multi-dimensional large sets of data. That's why it uses different graphical representations.

By extension, all widgets that present any kind of data is called a :ref:`View`. This applies to the following forms of data representations:

- in raw format (as given by the original strings in the text files, for example)
- in a graphical representation
- as the result of computations done on the raw data.


.. warning:: In the Squey terminology, these graphical and non-graphical representations are called :ref:`View`\ s.


The next screenshot gives a sample of the most significant :ref:`View`\ s in Squey:

.. image:: /_static/images/views_small.png
   :width: 1200

.. _Graphical Views:

Graphical Views
~~~~~~~~~~~~~~~

Squey provides different :ref:`Graphical Views` to help the user interact with its data or get an idea of what this data express.

- the :ref:`Global Parallel Coordinates View`
- the :ref:`Zoomed Parallel Coordinates View`
- the :ref:`Scatter Plot View`
- the :ref:`Series View`
- the :ref:`Hit Count View`

Non-Graphical Views
~~~~~~~~~~~~~~~~~~~

As said before, some of the :ref:`View`\ s in Squey are non-graphical.

Today, this terminology encompasses the following :ref:`View`\ s:

- the :ref:`Listing View`
- the :ref:`Layer Stack View`
- the :ref:`Statistics Views`


List of all Views in Squey
~~~~~~~~~~~~~~~~~~~~~~~~~~


As of the current version of Squey, the available :ref:`View`\ s are the following:

- the :ref:`Listing View`
- the :ref:`Layer Stack View`
- the :ref:`Global Parallel Coordinates View`
- the :ref:`Zoomed Parallel Coordinates View`
- the :ref:`Series View`
- the :ref:`Scatter Plot View`
- the :ref:`Hit Count View`
- the :ref:`Statistics Views`:
- the :ref:`Distinct Values dialog`
- the :ref:`Count by dialog`
- the :ref:`Sum by dialog`


The aim of the next chapters is to present in detail all the :ref:`View`\ s that are available in Squey.


