Axes binding
------------

The axes binding feature provides a way to propagate selections between sources and views using basic rules.
It can be used to work seamlessly with different source types.

When an axis from a source is bound to a compatible axis from another source, every selection made on the origin source is propagated on the current layer of the destination source.

As an example, if the binding is done on ipv4 axes, all the ipv4 selected on the current layer will automatically be selected on the current layer of the destination source.

An axes binding is enabled when two axes are bound together :

.. image:: /_static/images/correlation_menu.png
   :width: 900

An icon with a toolip is then displayed on the header of the origin column :

.. image:: /_static/images/correlation_tooltip.png
   :width: 900

Two axes binding modes are available
  #. Distinct : 
  #. Range

Current limitations :
  #. types "time", "float" and "double" are not supported yet
  #. Only direct axes bindings are supported

