
Concepts in brief
-----------------

This chapter presents all the concepts that are at the core of Squey. The understanding of each and every concept is not mandatory to start using the software but you are encouraged to read it beforehand.

The organisation of this chapter is more logical than alphabetical but to ease the use of this chapter as a reference, an alphabetically ordered list of links to concepts is provided below :

* :ref:`Axes combination`
* :ref:`Axis`
* :ref:`Column`
* :ref:`Data collection`
* :ref:`Event`
* :ref:`Field`
* :ref:`Filter`
* :ref:`Format`
* :ref:`Input`
* :ref:`Invalid event`
* :ref:`Investigation`
* :ref:`Layer`
* :ref:`Line property`
* :ref:`Mapping`
* :ref:`Scaling`
* :ref:`Selectable event`
* :ref:`Selection`
* :ref:`Source`
* :ref:`View`
* :ref:`Workspace`
* :ref:`Zombie event`
* :ref:`Zone`

.. _Input:

Input
~~~~~

An :ref:`Input` is a the smallest supported dataset unit such as:

#. :ref:`Text files`
#. :ref:`Apache Parquet files`
#. :ref:`PCAP files`
#. :ref:`Database queries`
#. :ref:`Elasticsearch queries`

One or more :ref:`Input`\ s are processed into a :ref:`Source` using a :ref:`Format` during the data ingest phase.

.. _Format:

Format
~~~~~~

A :ref:`Format` describes how one or more :ref:`Input`\ s will be processed and represented as a :ref:`Source`.
It gives the logic about how the :ref:`Event`\ s are split into
:ref:`Field`\ s; it gives meta-informations about :ref:`Field`\ s too, such as:

* their type
* their displayed name
* their associated :ref:`Mapping`;
* their associated :ref:`Scaling`;
* and a few more parameters.

A :ref:`Format` also provides an :ref:`Axes combination`.

.. note::

    :ref:`Format`\ s are internally managed for every :ref:`Input`\ s that are not a Text files and you won't even see them.

.. _Source:

Source
~~~~~~

A :ref:`Source` is the association of one or many :ref:`Input`\ s and a :ref:`Format`. Is it the equivalent of a worksheet in a spreadsheet software.

As an example, a set of Text files with the same structure, associated with a :ref:`Format` specifying how to extract the wanted :ref:`Field`\ s forms a :ref:`Source`.

.. _Event:

Event
~~~~~

An :ref:`Event` is a coherent data unit of a :ref:`Source` containing one value of each :ref:`Column`.

.. note::

    It is represented as a row on the :ref:`Listing View`, as a broken line on the :ref:`Global Parallel Coordinates View`, as a point on a :ref:`Scatter Plot View` and participate to the positionning of a data point in the :ref:`Series View` and :ref:`Hit Count View`.

.. _Invalid event:

Invalid event
~~~~~~~~~~~~~

An :ref:`Invalid Event` is an :ref:`Event` that failed to be properly processed by a :ref:`Format`.

:ref:`Invalid Event`\ s can be generated on two distinct cases :

#. When a discrepency is present in the :ref:`Input`
#. When there is a mismatch between the :ref:`Format` logic and the data contained in the :ref:`Input`. Usually, the :ref:`Format` can be refined to reduce and/or avoid these.

.. note::

    :ref:`Invalid Event`\ s are never discarded silently and are always listed in the "Error" tab.

.. _Invalid value:

Invalid value
~~~~~~~~~~~~~

Even in case of a valid :ref:`Event`, it can also happen that one or more values do not conform to the expected type (eg. a string is encountered instead of a number).

In such a case, the :ref:`Invalid value` is nevertheless displayed but with a noticeable accent, eg:

* Displayed in italic in the :ref:`Listing view` and the :ref:`Distinct values dialog`
* Displayed at specific place in the bottom of the :ref:`Axis`

They can also be searched specifically by the search feature.

.. _Field:

Field
~~~~~

A :ref:`Field` is a specific part of an :ref:`Event`.

The complete splitting process of an :ref:`Event` into smaller :ref:`Field`\ s is described in the :ref:`Format` associated to one or more :ref:`Input`\ s.



.. _Column:

Column
~~~~~~

A :ref:`Column` is an ordered list of all the values contained in a given :ref:`Field` in a given :ref:`Source`.


.. _Axis:

Axis
~~~~

An :ref:`Axis` is the mathematical representation of a :ref:`Column` found in a given :ref:`Source`. It is then completely tied to the :ref:`Column` it represents.

The term :ref:`Axis` will often come when dealing with graphical representations called :ref:`View`\ s.

A paire of :ref:`Axis` forms a :ref:`Zone`.


.. _Mapping:

Mapping
~~~~~~~

A :ref:`Mapping` is a first mathematical function that is applied to all the values of a :ref:`Column` so that they can be **ordered** on the associated :ref:`Axis`.

Squey offers different :ref:`Mapping` functions. Some are generic and can be applied to any type of values of :ref:`Field`\ s. Some are only meaningfull on specific types of :ref:`Field`\ s.

.. note::

    :ref:`Mapping`\ s can be changed in real time during an analysis.

.. _Scaling:

Scaling
~~~~~~~~

A :ref:`Scaling` is a second mathematical function applied on ordered values and determines how values are **scaled** over the axis.

There are three :ref:`Scaling` functions:

- Min/max : uses a **linear** scale
- Log Min/max : uses a **logarithmic** scale
- Uniform : uses a **uniform** scale

.. note::

    :ref:`Scaling`\ s can be changed in real time during an analysis.

.. _Axes combination:


Axes combination
~~~~~~~~~~~~~~~~

An :ref:`Axes combination` is an arbitrary multi-set of available axes. It is a convenient way to express alternative ordering of axes (as well as removal and/or duplication) in order to make analyses more fluid.

.. _View:

View
~~~~

A :ref:`View` is a widget that present and allow to manipulate any kind of data.

See :ref:`Views in Squey`.

.. _Selection:

Selection
~~~~~~~~~

A :ref:`Selection` is a subset of selected :ref:`Event`\ s of the current :ref:`Layer`.

:ref:`Selection`\ s are the core of the interactivity in Squey because :ref:`View`\ s are updated in real time when they change allowing thereby to conduct deep analyses.

.. _Layer:

Layer
~~~~~

A layer is a saved :ref:`Selection`.

.. _Investigation:

Investigation
~~~~~~~~~~~~~

:ref:`Investigation`\ s are meant to allow the users to save their work and be able to quickly resume their analysis later on. It allows them to save:

- all the :ref:`Layer`\ s and their states
- the ingested processed data
- a connection to the original data in order to be able to export subsets of it

.. note::

    Reloading an :ref:`Investigation` is way faster than reimporting the original data again because the ingestion phase will be skipped out.

.. _Workspace:

Workspace
~~~~~~~~~

A :ref:`Workspace` is a dockable area containing all the displayed :ref:`View`\ s of a :ref:`Source`.

.. _Data collection:

Data collection
~~~~~~~~~~~~~~~
A :ref:`Data collection` is a convenience to manually regroup different :ref:`Source`\ s according to arbitrary criteria. These criteria can be duration covered by the :ref:`Source`\ s, the sources types, etc.

.. _Zombie event:

Zombie event
~~~~~~~~~~~~

A :ref:`Zombie Event` is an :ref:`Event` which does not belong to the current
:ref:`Layer`.

.. _Selectable event:

Selectable event
~~~~~~~~~~~~~~~~

An :ref:`Event` is selectable if it belongs to the current layer event set.

.. _Line Property:

Line Property
~~~~~~~~~~~~~

A :ref:`Line property` is a property set attached to the :ref:`Event`\ s of a
:ref:`Layer`, such as their colors or their selectability.



.. _Filter:

Filter
~~~~~~

A :ref:`Filter` is an algorithm used to alter the :ref:`Line property` of the current active
:ref:`Layer` of the :ref:`Layer Stack View`.

.. _Zone:

Zone
~~~~

A :ref:`Zone` is a pair of Axes or of :ref:`Column`\ s.
