.. _Scatter Plot View:

Scatter Plot View
-----------------

This graphic :ref:`View` provides an alternative zone representation using cartesian coordinates
instead of parallel coordinates.

.. image:: /_static/images/scatter_view.png
   :width: 800

For comparison, here is the same zone represented using parallel coordinates:

.. image:: /_static/images/parallel_view_zone.png
   :width: 800

Selection modes
~~~~~~~~~~~~~~~

In order to make it easier to select events, three selections modes are avaible. They can be
switched from the view toolbar or directly with their keyboard shortcuts.

.. image:: /_static/images/scatter_view_toolbar.png
   :width: 300

Zoom
~~~~

As usual, zoom level can be changed using the mouse scroll button.

.. image:: /_static/images/scatter_view_zoom.png
   :width: 600

Accessing the view
~~~~~~~~~~~~~~~~~~

There are three ways to access this graphic view.

By right-clicking on the header of a zone on the parallel view:

.. image:: /_static/images/create_scatter_view_01.png
   :width: 400

By right-clicking on the header of column in the listing view:

.. image:: /_static/images/create_scatter_view_02.png
   :width: 500

By clicking on the scatter view button located on the workspace toolbar and selection the
appropriate zone:


.. |ico1| image:: /_static/images/create_scatter_view_03a.png
.. |ico2| image:: /_static/images/create_scatter_view_03b.png

|ico1| |ico2|

