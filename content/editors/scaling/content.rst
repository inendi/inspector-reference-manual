
Scaling
~~~~~~~~

This editor enables changing the current scaling configuration.

.. image:: /_static/images/datatree_edit_scaling.png
   :width: 300

.. image:: /_static/images/scaling_editor.png
   :width: 700

It allows to change, for each axis, the settings initially provided by the format:

* its scaling
