
Mapping
~~~~~~~
This editor enables changing the current mapping configuration.

.. image:: /_static/images/datatree_edit_mapping.png
   :width: 300

.. image:: /_static/images/mapping_editor.png
   :width: 700

It allows to change, for each axis, the settings initially provided by the format:

* its type
* its mapping
